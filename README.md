# PyETL

Sample usage example:

# Load

import pyetl

pyetl.load("file_path")

pyetl.load("file_path", headers=[list of header names])

# YourReaderClass must extend the Reader class
pyetl.load("file_path", headers=[list of header names], reader=YourReaderClass)

# Transform

import pyetl

rows = pyetl.transform('file_path',
                               skip_values=['-'], skip_columns=['engine-location', 'num-of-cylinders',
                                                                'engine-size', 'weight', 'horsepower',
                                                                'aspiration', 'price', 'make'],
                               apply=[('horsepower', lambda x: int(x.replace(",", "")))])
                               
Parameters
:data_file = The full path of the file or connection string which is passed to the reader class
:headers = Headers for the data table
:reader = is the File Reader or Stream Reader class which extends from the Reader class
:cell_validator = Cell Validator Class. If not provided then then Generic Cell Validator is used.
:skip_values = The rows to be skipped while this values are present either of any one of the skip columns
:skip_columns = Columns to check for skip values
:coerce = When conversion failed what to do
:apply = Apply arguments to apply on columns. [(column_name, callable)]

The pyetl package has no external libraries dependancy.

pyetl is the main package and pyetl.exposer.functions contains required functions to be accessible by users.

The package is highly extendible and modifiable and generic in most cases.

The load method loads data from data source whether the source is file or connection string

One can provide data reader, headers etc to the method.

The transform method takes several number of arguments which are used to configure and customize the behavior as per required.

One can pass data reader, data cell validator, apply functions etc and the DataTable will automatically apply based on the passed arguments.

The transform method actually create a DataTable and can return either the table or list of data by switching a parameter value return_data_table.

The tests/* contains three test cases only. 

I am actually going through rush here and I am really sorry for not writting all unit tests and other tests. 
I would request to allow me to talk to you in the following interview so you can understand my skills better.