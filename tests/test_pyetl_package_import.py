import unittest

from pyetl.pyio.csv_writter import to_csv


class TestPyETLPackageImport(unittest.TestCase):

    def setUp(self):
        pass

    def test_pyetl_package_import(self):
        import pyetl
        # etl = pyetl.load('/media/codenginebd/MyWorksandStuffs3/Projects/pyetl/Challenge_me.txt')
        # etl.head()
        rows = pyetl.transform('/media/codenginebd/MyWorksandStuffs3/Projects/pyetl/Challenge_me.txt',
                               skip_values=['-'], skip_columns=['engine-location', 'num-of-cylinders',
                                                                'engine-size', 'weight', 'horsepower',
                                                                'aspiration', 'price', 'make'],
                               apply=[('horsepower', lambda x: int(x.replace(",", "")))])
        # print(dt[1])
        # print(rows)
        # dt.head(n=100, columns=[0, 1])
        self.assertTrue(True)
        to_csv('/media/codenginebd/MyWorksandStuffs3/Projects/pyetl/Challenge_me.csv', rows)

    def teardown(self):
        pass
