import os
import unittest


loader = unittest.TestLoader()
start_dir = os.getcwd()
suite = loader.discover(start_dir)

runner = unittest.TextTestRunner()


if __name__ == "__main__":
    runner.run(suite)