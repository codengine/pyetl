import unittest
from pyetl.pyio.csv_writter import to_csv


class TestPyETLPackageImport(unittest.TestCase):

    def setUp(self):
        pass

    def test_load(self):
        import pyetl
        import os
        headers, rows = pyetl.load(os.path.join(os.getcwd(), "Challenge_me.txt"))
        self.assertTrue(type(headers) is list)
        self.assertTrue(type(rows) is list)

    def test_transform(self):
        import pyetl
        import os
        rows = pyetl.transform(os.path.join(os.getcwd(), "Challenge_me.txt"),
                               skip_values=['-'], skip_columns=['engine-location', 'num-of-cylinders',
                                                                'engine-size', 'weight', 'horsepower',
                                                                'aspiration', 'price', 'make'],
                               apply=[('horsepower', lambda x: int(x.replace(",", "")))])
        self.assertTrue(type(rows) is list)

    def test_to_csv(self):
        import os
        data = [
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            [11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        ]
        to_csv(os.path.join(os.getcwd(), "Challenge_me.csv"), data)

    def teardown(self):
        pass