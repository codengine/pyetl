from pyetl.entities.data_table import DataTable
from pyetl.interfaces.etl_abstract import AbstractETL
from pyetl.utils.file_util import get_default_file_reader


class PyETL(AbstractETL):
    def load_data_file(self, data_file, headers=[], reader=None):
        if not reader:
            FileReaderClass = get_default_file_reader(data_file)
        else:
            FileReaderClass = reader

        reader_instance = FileReaderClass()
        self._etl_data = reader_instance.read(file_path=data_file)

    def etl_transform(self, data_file, headers=[], columns=[], reader=None,
                      cell_validator=None, skip_values=[], skip_columns=[], coerce=False, apply=[]):
        if not reader:
            FileReaderClass = get_default_file_reader(data_file)
        else:
            FileReaderClass = reader

        reader_instance = FileReaderClass()
        self._headers, self._etl_data = reader_instance.read(file_path=data_file)

        self._data_table = DataTable(headers=self._headers, columns=columns, data=self._etl_data, cell_validator=cell_validator,
                                     skip_values = skip_values, skip_columns = skip_columns,
                                     coerce=coerce, apply=apply)

        return self._data_table
