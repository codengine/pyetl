from datetime import datetime
from pyetl.entities.data_row import DataRow
from pyetl.exceptions.etl_exception import ETLException
from pyetl.validators.data_cell_validator import DataTableCellValidator, GenericValidator


class DataTable(object):
    def __init__(self, headers = [], data=[], cell_validator=None,
                 skip_values=[], skip_columns=[], coerce=False, apply=[]):
        if cell_validator:
            self._cell_validator = cell_validator
        else:
            self._cell_validator = GenericValidator
        self._data_types = {} #  {0: [int, float], 1: [str, int]}
        self._headers = headers[0] if headers else []
        self._headers_dict = {}
        self._header_index_mapping = {}
        self._table_data = []
        self._table_rows = []
        self._column_count = len(headers) if headers else 0
        self._skip_column_indexes = []
        self._apply_passed = {}

        untitled_column_index_dict = {}

        for index, hname in enumerate(self._headers):

            if hname and hname in skip_columns:
                self._skip_column_indexes += [index]

            if not hname:
                hname = "Untitle Column"
                untitled_column_index = untitled_column_index_dict.get(hname)
                if not untitled_column_index:
                    untitled_column_index_dict[hname] = 1
                else:
                    hname = "Untitle Column_%s" % untitled_column_index_dict[hname]
                    untitled_column_index_dict[hname] += 1
            else:
                untitled_column_index = untitled_column_index_dict.get(hname)
                if not untitled_column_index:
                    untitled_column_index_dict[hname] = 1
                else:
                    hname = hname + "_%s" % untitled_column_index
                    untitled_column_index_dict[hname] += 1

            self._headers_dict[index] = hname
            self._header_index_mapping[hname] = index

        for i, row in enumerate(data):
            if i == 0 and not headers:
                self._column_count += 1
                self._headers = row

            skip_row = False
            for cindex, cell_value in enumerate(row):
                if skip_values and cell_value.strip() in skip_values:
                    if cindex in self._skip_column_indexes:
                        skip_row = True
                        break

            if skip_row:
                continue

            table_row = DataRow(data=row, cell_validator=self._cell_validator)
            row_types = table_row.row_types()

            for rindex, cell_type in row_types.items():
                if not self._data_types.get(rindex):
                    self._data_types[rindex] = [cell_type]
                else:
                    self._data_types[rindex] += [cell_type]
                    self._data_types[rindex] = list(set(self._data_types[rindex]))

            cleaned_cell_data = table_row.cleaned_data
            self._table_data += [cleaned_cell_data]
            self._table_rows += [table_row]

        # Now convert values which have more than two types found.
        for column_index, dtypes in self._data_types.items():
            column_name = self._headers_dict.get(column_index)
            if len(dtypes) > 1:
                _dtype = self.get_type_from_list(dtypes)
                self.convert_to(column_name, _dtype, coerce=coerce)
                self._data_types[column_index] = [_dtype]

        # Now apply all the apply aruments if passed.
        if apply:
            for apply_argument in apply:
                if len(apply_argument) != 2:
                    raise ETLException("Apply must pass two parameter: e.g apply=[(column_name, callable)]")
                apply_column_name = apply_argument[0]
                apply_callable = apply_argument[1]
                apply_column_index = self._header_index_mapping.get(apply_column_name)
                if not apply_column_index:
                    raise ETLException("Invalid column name: %s" % apply_column_name)
                if not callable(apply_callable):
                    raise ETLException("Apply must be passed with callable argument.")
                self._apply_passed[apply_column_index] = apply_callable

        for cindex, apply_callable in self._apply_passed.items():
            self.apply(cindex, apply_callable)

    def get_type_from_list(self, type_list):
        data_type_priority = {
            int: 1,
            float: 2,
            str: 3
        }
        priority_mapping_data_type = {
            1: int,
            2: float,
            3: str
        }
        type_priority = 0
        for i, _type_name in enumerate(type_list):
            _type_priority = data_type_priority.get(_type_name) or 0
            if _type_priority > type_priority:
                type_priority = _type_priority

        return priority_mapping_data_type.get(type_priority)

    def apply(self, column_name_or_index, f_name, coerce_failure=True, *args, **kwargs):
        if not callable(f_name):
            raise ETLException("Argument must be callable")
        if type(column_name_or_index) is int and \
                (column_name_or_index >= 0 and column_name_or_index < len(self._headers_dict)):
            column_index = column_name_or_index
        elif type(column_name_or_index) is str:
            column_index = self._header_index_mapping.get(column_name_or_index)
            if not column_index:
                raise ETLException("Invalid column name: %s" % column_name)
        else:
            raise ETLException("Column name must be index or name")

        for index, row in enumerate(self._table_data):
            value = row[column_index]
            try:
                value = f_name(value)
                if value:
                    row[column_index] = value
            except Exception as exp:
                if not coerce_failure:
                    raise ETLException("Callable calling failed: %s" % f_name.__name__)
                else:
                    row[column_index] = None

    def convert_to(self, column_name, type_name, coerce=False, *args, **kwargs):
        column_index = self._header_index_mapping.get(column_name)
        if not column_index:
            raise ETLException("Invalid column name: %s" % column_name)

        valid_types = [str, int, float, datetime]

        if type_name not in valid_types:
            raise ETLException("Invalid conversion type: %s" % type_name)

        for index, row in enumerate(self._table_data):
            value = row[column_index]

            if type_name == int:
                try:
                    value = int(value)
                    row[column_index] = value
                except:
                    if coerce:
                        row[column_index] = None
                    else:
                        raise ETLException("Invalid value to convert: %s" % value)
            elif type_name == float:
                try:
                    value = float(value)
                    row[column_index] = value
                except:
                    if coerce:
                        row[column_index] = None
                    else:
                        raise ETLException("Invalid value to convert: %s" % value)
            elif type_name == datetime:
                if not args:
                    raise ETLException("No format string specified")
                format_string = args[0]
                try:
                    value = datetime.strptime(format_string)
                    row[column_index] = value
                except:
                    if coerce:
                        row[column_index] = None
                    else:
                        raise ETLException("Invalid value to convert: %s" % value)

    @property
    def headers(self):
        return [self._headers]

    @property
    def rows(self):
        return self._table_data

    def perform_clean_operation(self):
        pass

    def scan(self,size=5):
        pass

    def __iter__(self):
        iter_data = self._headers + self._table_data
        return iter(iter_data)

    def __getitem__(self, key):
        _table_data = self._headers + self._table_data
        if isinstance(key, slice):
            return [_table_data[i] for i in range(key.start, key.stop, key.step)]
        return _table_data[key]

    def __setitem__(self, key, data):
        self._table_data.__setitem__(key, data)

    def __delitem__(self, key):
        self._table_data.__delitem__(key)

    def head(self, n=5, columns=[]):
        split_data = self._table_data[:n]
        if columns:
            print_data = []
            print_headers = []
            for i, row in enumerate(split_data):
                print_cols = []
                for column in columns:
                    print_cols += [row[column]]
                print_data += [print_cols]
            if self._headers:
                for column in columns:
                    print_headers += [self._headers[0][column]]
            print(print_headers)
            print(print_data)
        else:
            print(self._headers)
            print(split_data)


