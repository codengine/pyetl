from pyetl.entities.data_cell import DataCell


class DataRow(object):
    def __init__(self, data=[], cell_validator=None):
        self._cell_validator = cell_validator
        self._data_types = {}
        self._row_data = data
        self._cleaned_data = []
        self._cells = []
        for index, cell_value in enumerate(data):
            data_cell = DataCell(index, value=cell_value, cell_validator=self._cell_validator)
            self._cell_type, self._cleaned_value = data_cell.clean()
            self._cleaned_data += [self._cleaned_value]
            self._data_types[index] = self._cell_type
            self._cells += [data_cell]

    def row_types(self):
        return self._data_types

    @property
    def cleaned_data(self):
        return self._cleaned_data

    @property
    def data_types(self):
        return self._data_types

    def __iter__(self):
        return iter(self._cleaned_data)

