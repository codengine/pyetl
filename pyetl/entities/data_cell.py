class DataCell(object):
    def __init__(self, index, value, cell_validator=None):
        self._cell_index = index
        self._cell_validator = cell_validator
        self._dtype = str
        self._value = value
        self.clean()

    def cell_type(self):
        return self._dtype

    def cell_value(self):
        return self._value

    def clean(self):
        if self._cell_validator:
            self._dtype, self._value = self._cell_validator.validate(self._cell_index, self._value)
        return type(self._value), self._value
