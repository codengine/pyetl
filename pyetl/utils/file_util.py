from pyetl.pyio.csv_reader import CSVFileReader
from pyetl.pyio.file_reader import FileReader


def get_default_file_reader(file_path):
    if file_path.endswith(".txt"):
        return CSVFileReader
