import csv
from functools import wraps


def _generate_file_reader_dsv(file_name, delimeter, encoding='utf-8', first_row_header=True, **kwargs):
    @wraps
    def read(self, file_path, delimeter, encoding, first_row_header, **kwargs):
        headers = []
        contents = []
        with open(file_path, newline='', encoding=encoding) as csvfile:
            spamreader = csv.reader(csvfile, delimiter=delimeter)
            for index, row in enumerate(spamreader):
                if index == 0:
                    if first_row_header:
                        headers += [row]
                    else:
                        contents += [row]
                else:
                    contents += [row]
        return headers, contents
    return read