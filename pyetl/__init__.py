from pyetl.exposer.functions import *

depedancies = []
missing_dependacies = []

for dependancy in depedancies:
    try:
        __import__(dependancy)
    except ImportError as e:
        missing_dependacies += [dependancy]

if missing_dependacies:
    raise ImportError("Missing required dependencies {0}".format(missing_dependencies))

del depedancies, missing_dependacies

# Module level doc string
__doc__ = """
PyETL is a python tool for ETL in data science based on specification 
ETL Challenge-20180604-v1.pdf
"""