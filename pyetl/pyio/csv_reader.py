import csv
from pyetl.interfaces.reader import Reader


class CSVFileReader(Reader):
    def read(self, file_path, first_row_header=True, encoding='utf-8'):
        headers = []
        contents = []
        with open(file_path, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';')
            for index, row in enumerate(spamreader):
                if index == 0:
                    if first_row_header:
                        headers += [row]
                    else:
                        contents += [row]
                else:
                    contents += [row]
        return headers, contents
