from pyetl.utils.decorators import _generate_file_reader_dsv


@_generate_file_reader_dsv
def read_csv(file_path, delimeter, encoding, first_row_header, **kwargs):
    pass