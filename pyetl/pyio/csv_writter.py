import csv


def to_csv(file_path, data=[]):
    with open(file_path, 'w') as csv_file:
        writter = csv.writer(csv_file, delimiter=',')
        writter.writerows(data)
