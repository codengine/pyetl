from pyetl.interfaces.reader import Reader


class FileReader(Reader):
    def read(self, file_path, encoding='utf-8'):
        contents = []
        with open(file_path, 'r', encoding='utf-8') as fp:
            contents = fp.readlines()
            contents = [line.rstrip('\n') for line in contents]
        return contents
