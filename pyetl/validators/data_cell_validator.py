from datetime import datetime
from dateutil import parser
from pyetl.interfaces.etl_validator import ETLValidator


class DataTableCellValidator(ETLValidator):

    @classmethod
    def validate(cls, index, value):
        if index == 0:
            return int(value)
        elif index == 1:
            return value.encode('utf-8') if type(value) is str else value
        return value


class GenericValidator(ETLValidator):
    @classmethod
    def validate(cls, index, value):
        if cls.is_float(value):
            return float, float(value)
        elif cls.is_int(value):
            return int, int(value)
        elif cls.is_datetime(value):
            return datetime, parser.parse(value)
        elif cls.is_string_numeric(value):
            n_value = cls.words_to_simple_number(value)
            return int, int(n_value)
        else:
            return str, value


