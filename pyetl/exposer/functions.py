from pyetl.entities.data_table import DataTable
from pyetl.exceptions.etl_exception import ETLException
from pyetl.utils.file_util import get_default_file_reader
from pyetl.validators.data_cell_validator import DataTableCellValidator
from pyetl.pyio.csv_writter import *

header_list, etl_data = [], []


"""
import pyetl

pyetl.load("file_path")

pyetl.load("file_path", headers=[list of header names])

# YourReaderClass must extend the Reader class
pyetl.load("file_path", headers=[list of header names], reader=YourReaderClass)

"""


def load(data_file, headers = [], reader = None):
    global header_list
    global etl_data
    if not reader:
        FileReaderClass = get_default_file_reader(data_file)
    else:
        FileReaderClass = reader
    try:
        reader_instance = FileReaderClass(data_file)
        header_list, etl_data = reader_instance.read(file_path=data_file)

        if headers:
            if len(headers) == len(headers):
                header_list = headers
            else:
                raise ETLException("All headers must be supplied")
    except ETLException as exp:
        return None, None

    return header_list, etl_data


"""
import pyetl

rows = pyetl.transform('file_path',
                               skip_values=['-'], skip_columns=['engine-location', 'num-of-cylinders',
                                                                'engine-size', 'weight', 'horsepower',
                                                                'aspiration', 'price', 'make'],
                               apply=[('horsepower', lambda x: int(x.replace(",", "")))])
                               
Parameters
:data_file = The full path of the file or connection string which is passed to the reader class
:headers = Headers for the data table
:reader = is the File Reader or Stream Reader class which extends from the Reader class
:cell_validator = Cell Validator Class. If not provided then then Generic Cell Validator is used.
:skip_values = The rows to be skipped while this values are present either of any one of the skip columns
:skip_columns = Columns to check for skip values
:coerce = When conversion failed what to do
:apply = Apply arguments to apply on columns. [(column_name, callable)]
"""


def transform(data_file, headers = [], reader = None, cell_validator = None,
              skip_values = [], skip_columns = [], coerce=False, apply=[],
              return_data_table=False):
    global header_list
    global etl_data
    if not headers and not etl_data:
        header_list, etl_data = load(data_file, headers = headers, reader = reader)

    _data_table = DataTable(headers=header_list, data=etl_data,
                            cell_validator=cell_validator,
                            skip_values=skip_values, skip_columns=skip_columns,
                            coerce=coerce, apply=apply)

    if return_data_table:
        return _data_table
    else:
        return _data_table.headers + _data_table.rows

