import json
from dateutil import parser
from abc import ABC, abstractmethod


class ETLValidator(ABC):

    @classmethod
    def words_to_number(cls, value):
        units = [
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
            "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
            "sixteen", "seventeen", "eighteen", "nineteen",
        ]

        tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

        scales = ["hundred", "thousand", "million", "billion", "trillion"]

        numwords["and"] = (1, 0)
        for idx, word in enumerate(units):  numwords[word] = (1, idx)
        for idx, word in enumerate(tens):       numwords[word] = (1, idx * 10)
        for idx, word in enumerate(scales): numwords[word] = (10 ** (idx * 3 or 2), 0)

        ordinal_words = {'first': 1, 'second': 2, 'third': 3, 'fifth': 5, 'eighth': 8, 'ninth': 9, 'twelfth': 12}
        ordinal_endings = [('ieth', 'y'), ('th', '')]

        value = value.replace('-', ' ')

        current = result = 0
        curstring = ""
        onnumber = False
        for word in value.split():
            if word in ordinal_words:
                scale, increment = (1, ordinal_words[word])
                current = current * scale + increment
                if scale > 100:
                    result += current
                    current = 0
                onnumber = True
            else:
                for ending, replacement in ordinal_endings:
                    if word.endswith(ending):
                        word = "%s%s" % (word[:-len(ending)], replacement)

                if word not in numwords:
                    if onnumber:
                        curstring += repr(result + current) + " "
                    curstring += word + " "
                    result = current = 0
                    onnumber = False
                else:
                    scale, increment = numwords[word]

                    current = current * scale + increment
                    if scale > 100:
                        result += current
                        current = 0
                    onnumber = True

        if onnumber:
            curstring += repr(result + current)

        return curstring

    @classmethod
    def words_to_simple_number(cls, value):
        units = [
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
            "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
            "sixteen", "seventeen", "eighteen", "nineteen",
        ]
        unit_dict = {v: i for i, v in enumerate(units)}
        return unit_dict.get(value.strip()) or value

    @classmethod
    def is_string_numeric(cls, value):
        units = [
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
            "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
            "sixteen", "seventeen", "eighteen", "nineteen",
        ]
        try:
            value = value.strip()
            return value in units
        except Exception as exp:
            return False

    @classmethod
    def is_number(cls, value):
        try:
            json.loads(value)
            return True
        except ValueError:
            return False

    @classmethod
    def is_datetime(cls, value):
        try:
            parser.parse(value)
            return True
        except Exception as exp:
            return False

    @classmethod
    def is_int(cls, value):
        try:
            value = json.loads(value)
            return type(value) == int
        except Exception as exp:
            return False

    @classmethod
    def is_float(cls, value):
        try:
            value = json.loads(value)
            return type(value) == float
        except Exception as exp:
            return False

    @classmethod
    @abstractmethod
    def validate(cls, index, value):
        pass
