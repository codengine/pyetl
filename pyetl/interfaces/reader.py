from abc import ABC, abstractmethod


class Reader(ABC):

    def __init__(self, connection_string=None):
        self._connection_string = connection_string

    @abstractmethod
    def read(self, file_path, encoding='utf-8'):
        pass