from abc import ABC, abstractmethod

from pyetl.utils.file_util import get_default_file_reader


class AbstractETL(ABC):
    def __init__(self):
        self._etl_data = []
        self._data_table = None

    @abstractmethod
    def load_data_file(self, data_file, headers=[], reader=None):
        pass

    @abstractmethod
    def etl_transform(self, data_file, headers=[], columns=[], reader=None,
                      cell_validator=None, skip_values=[], skip_columns=[]):
        return self._etl_data
